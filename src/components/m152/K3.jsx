import React from 'react';
import jpg from '../../media/m152_image.jpg';
import png from '../../media/m152_image.png';
import gif from '../../media/m152_image.gif';
import video from '../../media/memevid.mp4';
import mp3 from '../../media/mp3.mp3';
import ogg from '../../media/ogg.ogg';
import wav from '../../media/wav.wav';

export default function K3() {
  return (
    <>
      <h1>K3</h1>
      <br />
      <h2>Images</h2>
      <div className='imageGrid'>
        <div className='imageCard'>
          <div className='imageText'>
            <h1>JPG</h1>
          </div>
          <div>
            <img src={jpg} alt='jpg' className='imageImage' />
          </div>
        </div>
        <div className='imageCard'>
          <div className='imageText'>
            <h1>PNG</h1>
          </div>
          <div>
            <img src={png} alt='png' className='imageImage' />
          </div>
        </div>
        <div className='imageCard'>
          <div className='imageText'>
            <h1>GIF</h1>
          </div>
          <div>
            <img src={gif} alt='gif' className='imageImage' />
          </div>
        </div>
      </div>
      <br />
      <p>
        Here you can see one of my personal edits. i tried to get some sort of
        supernatural feelings out of it.
      </p>
      <br />
      <h2>Video</h2>
      <video width='320' height='240' controls>
        <source src={video} type='video/mp4' />
        Your browser does not support the video tag.
      </video>
      <br />
      <p>
        Here i edited a fighting scene with a friend. i replaced the greenscreen
        with a own background.
      </p>
      <br />
      <h2>Audio</h2>
      <h3>MP3</h3>
      <audio controls>
        <source src={mp3} type='audio/mpeg' />
        Your browser does not support the audio element.
      </audio>

      <h3>WAV</h3>
      <audio controls>
        <source src={wav} type='audio/wav' />
        Your browser does not support the audio element.
      </audio>

      <h3>OGG</h3>
      <audio controls>
        <source src={ogg} type='audio/ogg' />
        Your browser does not support the audio element.
      </audio>
      <br />
      <br />
      <p>Here you can hear one of my favorite beatbox songs of all time!</p>
    </>
  );
}
