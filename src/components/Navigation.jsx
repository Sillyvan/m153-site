import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = () => {
  return (
    <>
      <nav className='navbar'>
        <ul className='navbar-nav'>
          {/* Title Element */}
          <li className='logo'>
            <Link to='#' className='nav-link'>
              <span className='link-text logo-text'>Gehrig.DEV</span>
              <svg
                aria-hidden='true'
                focusable='false'
                data-prefix='fad'
                data-icon='angle-double-right'
                role='img'
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 448 512'
                className='svg-inline--fa fa-angle-double-right fa-w-14 fa-5x'
              >
                <g className='fa-group'>
                  <path
                    fill='currentColor'
                    d='M224 273L88.37 409a23.78 23.78 0 0 1-33.8 0L32 386.36a23.94 23.94 0 0 1 0-33.89l96.13-96.37L32 159.73a23.94 23.94 0 0 1 0-33.89l22.44-22.79a23.78 23.78 0 0 1 33.8 0L223.88 239a23.94 23.94 0 0 1 .1 34z'
                    className='fa-secondary'
                  ></path>
                  <path
                    fill='currentColor'
                    d='M415.89 273L280.34 409a23.77 23.77 0 0 1-33.79 0L224 386.26a23.94 23.94 0 0 1 0-33.89L320.11 256l-96-96.47a23.94 23.94 0 0 1 0-33.89l22.52-22.59a23.77 23.77 0 0 1 33.79 0L416 239a24 24 0 0 1-.11 34z'
                    className='fa-primary'
                  ></path>
                </g>
              </svg>
            </Link>
          </li>

          {/* Nav Childrens */}
          <li className='nav-item'>
            <Link to='/' className='nav-link'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                width='48px'
                height='48px'
              >
                <path
                  d='M19,11v9h-5v-6h-4v6H5v-9H3.6L12,3.4l8.4,7.6H19z'
                  opacity='.3'
                  fill='currentColor'
                  className='fa-secondary'
                />
                <path
                  fill='currentColor'
                  d='M20,21h-7v-6h-2v6H4v-9H1l11-9.9L23,12h-3V21z M15,19h3v-8.8l-6-5.4l-6,5.4V19h3v-6h6V19z'
                  className='fa-primary'
                />
              </svg>
              <span className='link-text'>Home</span>
            </Link>
          </li>

          <li className='nav-item'>
            <Link to='/about' className='nav-link'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                width='48px'
                height='48px'
              >
                <path
                  d='M18.92,9C18.434,5.609,15.526,3,12,3S5.566,5.609,5.08,9H18.92z'
                  opacity='.3'
                  fill='currentColor'
                  className='fa-primary'
                />
                <path
                  fill='currentColor'
                  d='M17,8H7c-1.654,0-3,1.346-3,3v1h2v-1c0-0.551,0.449-1,1-1h10c0.551,0,1,0.449,1,1v1h2v-1C20,9.346,18.654,8,17,8z'
                  className='fa-primary'
                />
                <path
                  fill='currentColor'
                  d='M20,11.767V10c0-4.411-3.589-8-8-8s-8,3.589-8,8v1.767C3.372,12.329,3,13.14,3,14c0,1.276,0.823,2.397,1.992,2.816C6.396,20.028,9.043,22,12,22s5.604-1.972,7.008-5.184C20.177,16.397,21,15.276,21,14C21,13.14,20.628,12.329,20,11.767z M18.137,14.981l-0.585,0.081l-0.211,0.551C16.302,18.319,14.256,20,12,20s-4.302-1.681-5.34-4.386l-0.211-0.551l-0.585-0.081C5.371,14.914,5,14.492,5,14c0-0.36,0.186-0.682,0.498-0.861L6,12.851V10c0-3.309,2.691-6,6-6s6,2.691,6,6v2.851l0.502,0.288C18.814,13.318,19,13.64,19,14C19,14.492,18.629,14.914,18.137,14.981z'
                  className='fa-primary'
                />
              </svg>
              <span className='link-text'>About Me</span>
            </Link>
          </li>
          <li className='nav-item'>
            <Link to='/socialmedia' className='nav-link'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                width='48px'
                height='48px'
              >
                <path
                  d='M19,20H5c-0.6,0-1-0.4-1-1V5c0-0.6,0.4-1,1-1h14c0.6,0,1,0.4,1,1v14 C20,19.6,19.6,20,19,20z'
                  opacity='.3'
                  fill='currentColor'
                  className='fa-secondary'
                />
                <path
                  fill='currentColor'
                  d='M19,21H5c-1.1,0-2-0.9-2-2V5c0-1.1,0.9-2,2-2h14c1.1,0,2,0.9,2,2v14C21,20.1,20.1,21,19,21z M19,19v1V19L19,19z M5,5v14h14 V5H5z'
                  className='fa-primary'
                />
                <path
                  fill='currentColor'
                  d='M7 15H17V17H7zM14 7H17V9H14zM12 11H17V13H12zM10.6 7C9.9 7 9.5 7.5 9.5 7.5S9.1 7 8.4 7C8 7 7.6 7.2 7.3 7.6c-1 1.3.9 2.8 1.3 3.2.3.2.6.5.8.7.1.1.2.1.3 0 .2-.2.5-.5.8-.7.4-.4 2.3-1.9 1.3-3.2C11.4 7.2 11 7 10.6 7z'
                  className='fa-primary'
                />
              </svg>
              <span className='link-text'>Social</span>
            </Link>
          </li>
          <li className='nav-item'>
            <Link to='/m152' className='nav-link'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                width='48px'
                height='48px'
              >
                <path
                  fill='currentColor'
                  d='M9 13.4L5.3 9.7 6.7 8.3 9 10.6 13.3 6.3 14.7 7.7z'
                  className='fa-primary'
                />
                <path
                  fill='currentColor'
                  className='fa-primary'
                  d='M16,2H4C2.9,2,2,2.9,2,4v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C18,2.9,17.1,2,16,2z M16,4v12H4V4H16z'
                />
                <path
                  fill='currentColor'
                  className='fa-secondary'
                  d='M6,20v2h14c1.1,0,2-0.9,2-2V6h-2v14H6z'
                />
                <path
                  fill='currentColor'
                  className='fa-primary'
                  d='M3 3H17V17H3z'
                  opacity='.3'
                />
              </svg>
              <span className='link-text'>M152</span>
            </Link>
          </li>
          {/* Last Children Element  (Remove from css to remove this)*/}
          <li className='nav-item'>
            <Link to='/impressum' className='nav-link'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                width='48px'
                height='48px'
              >
                <path
                  fill='currentColor'
                  d='M12 3A9 9 0 1 0 12 21A9 9 0 1 0 12 3Z'
                  className='fa-secondary'
                />
                <path
                  fill='currentColor'
                  d='M13,17h-2v-6h2V17z M13,9h-2V7h2V9z'
                  className='fa-primary'
                />
                <path
                  fill='currentColor'
                  d='M12,22C6.5,22,2,17.5,2,12C2,6.5,6.5,2,12,2c5.5,0,10,4.5,10,10C22,17.5,17.5,22,12,22z M12,4c-4.4,0-8,3.6-8,8 c0,4.4,3.6,8,8,8c4.4,0,8-3.6,8-8C20,7.6,16.4,4,12,4z'
                  className='fa-primary'
                />
              </svg>
              <span className='link-text'>Legal</span>
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Navigation;
