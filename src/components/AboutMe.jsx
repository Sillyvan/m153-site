import React from 'react';

export default function AboutMe() {
  const calculate_age = date => {
    var diff_ms = Date.now() - date.getTime();
    var age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  };
  return (
    <div>
      <h1>About Me</h1>
      <p>
        Im Silvan Gehrig. Im {calculate_age(new Date(1999, 11, 20))} years old
        and i live in beautiful Switzerland.
      </p>
      <br />
      <p>
        I have been a tech enthusiast all my life long. Already as a child i
        screwed around with toys and i had alot of fun (my parents didnt)
      </p>
      <br />
      <br />
      <p>
        Currently im working as a full stack dev for Inspectron AG in thalwil. I
        am usually working with ReactJS for the Frontend, NodeJS for the Backend
        and MariaDB for my Database
      </p>
      <br />
      <br />
      <p>
        In my freetime i like to skate with friends or play videogames with
        them. My favorite animal is the cat and thats reason why i own 2
      </p>
    </div>
  );
}
