import React from 'react';

const Impressum = () => {
  return (
    <>
      <div>
        <h2>Kontaktadresse</h2>
        <p>
          Silvan Gehrig
          <br />
          Sagenmattstrasse 11
          <br />
          6344 Meierskappel
          <br />
          Schweiz
          <br />
          silvan.gehrig@bluewin.ch
          <br />
          0418554982
        </p>
        <h2>Vertretungsberechtigte Personen</h2>
        <p>Silvan Gehrig, CEO</p>
        <h2>Haftungsausschluss</h2>
        <p>
          Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen
          Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und
          Vollständigkeit der Informationen. Haftungsansprüche gegen den Autor
          wegen Schäden materieller oder immaterieller Art, welche aus dem
          Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten
          Informationen, durch Missbrauch der Verbindung oder durch technische
          Störungen entstanden sind, werden ausgeschlossen. Alle Angebote sind
          unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der
          Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu
          verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise
          oder endgültig einzustellen.
        </p>
        <h2>Haftung für Links</h2>
        <p>
          Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres
          Verantwortungsbereichs Es wird jegliche Verantwortung für solche
          Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten
          erfolgen auf eigene Gefahr des Nutzers oder der Nutzerin.
        </p>
        <h2>Urheberrechte</h2>
        <p>
          Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder
          anderen Dateien auf der Website gehören ausschliesslich der Firma
          Gehrig oder den speziell genannten Rechtsinhabern. Für die
          Reproduktion jeglicher Elemente ist die schriftliche Zustimmung der
          Urheberrechtsträger im Voraus einzuholen.
        </p>
        <h2>Quelle</h2>
        <p>
          Dieses Impressum wurde am 13.03.2020 mit dem Impressum Generator der
          Webdesign Agentur{' '}
          <a
            href='https://webkoenig.ch/'
            target='_blank'
            rel='noopener noreferrer'
          >
            Webkönig{' '}
          </a>
          erstellt. Die Agentur Webkönig übernimmt keine Haftung.
        </p>
        <div style={{ marginTop: '3rem' }}>
          <h1>Urheberrecht Erklärung (K2)</h1>
          <br />
          <h2>Copyright</h2>
          <p>
            Bei Copyright kann der Ersteller bzw. der Autor entscheiden, ob er
            das Produkt Kosenlos oder Kostenpflichtig zur verfügung stellen
            möchte. <br /> <b>Jedoch</b> kann der Autor selber entscheiden, wie
            und wo das Produkt eingesetzt werden darf auch wenn man für das
            Produkt Geld bezahlt hat!
          </p>
          <br />
          <h2>Public Domain</h2>
          <p>
            Bei Public Domain gibt es keine einschränkugen. Man darf das Produkt
            so einsetzen wie man dies gerne möchte.
          </p>
          <br />
          <h2>Fair Use</h2>
          <p>
            Bei Fair Use darf nicht Jedermann die Ressource benutzen. Die
            Ressource darf hier nämlich nur für bildende und mediale Zwecke
            eingesetzt werden.
          </p>
        </div>
        <br />
        <br />
        <br />
        <h3>Quellenverzeichnis</h3>
        <br />
        Schriftart: Google Roboto (Public Domain) <br />
        Media: Silvan Gehrig
        <br />
        <br />
        <br />
        <h3>
          All rights reserved. All Media Content is selfmade and owned by Silvan
          Gehrig
        </h3>
      </div>
    </>
  );
};
export default Impressum;
