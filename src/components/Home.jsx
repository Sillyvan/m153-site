import React, { Component } from 'react';

export default class Home extends Component {
  render() {
    return (
      <div>
        <h1>Home</h1>
        <p>Welcome to Gehrig.DEV!</p>
        <p>Let me introduce you a little bit into my life and the work i do</p>
        <br />
        <p>
          On this page you can find my personal style and my preferd colors.
          This Website was made with ReactJS my preferd Frontend library.
          <br />
          Check out the other pages to find more information about me.
        </p>
        <br />
      </div>
    );
  }
}
