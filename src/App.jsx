import React from 'react';
import './App.css';
import Routing from './Routing';
import Navigation from './components/Navigation';

function App() {
  return (
    <div>
      <Navigation />
      <main>
        <Routing />
      </main>
    </div>
  );
}

export default App;
