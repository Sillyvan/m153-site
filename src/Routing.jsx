import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import AboutMe from './components/AboutMe';
import SocialMedia from './components/SocialMedia';
import Impressum from './components/Impressum';
import M152 from './components/M152';

const Routing = () => {
  return (
    <>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/about' component={AboutMe} />
        <Route exact path='/socialmedia' component={SocialMedia} />
        <Route exact path='/impressum' component={Impressum} />
        <Route exact path='/m152' component={M152} />
      </Switch>
    </>
  );
};

export default Routing;
